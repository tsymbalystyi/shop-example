import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

const store = new Vuex.Store({
   plugins: [createPersistedState({
    paths: ['cart']
    })],
   state: {
     cart: [],
   },
   mutations: {
       addToCart: (state, product) => state.cart.push(product),
   },
   getters: {
       getCart: (state) =>
       {
           return state.cart
       },
       getTotalPrice: (state) =>
       {
           return state.cart.reduce((total, product)=>{
            return total + product.price;
           }, 0).toFixed(2)
       },
   }
 })
 export default store;